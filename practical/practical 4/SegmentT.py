def build(s,e,node):
	if(s==e):
		stree[node] = arr[s]
	m = (s+e)//2
	left = build(s,m, 2*node+1)
	right = build(m+1, e, 2*node+2)
	stree[node] = left + right
	return stree[node]

def query(s,e,l,r,node):
	if l>e or r<s:
		return 0
	if(l<=s and r>=e):
		return stree[node]
	m = (s+e)//2
	left = query(s,m, l,r,2*node+1)
	right = query(m+1,e,l,r,2*node+2)

def buildTree():
	return build(0,n-1,0)
def querySum():
	return query(0,n-1,l,r,0)


if __name__ == '__main__':
	arr = [3 2 4 5 1 1 5 3]
	n = len(arr)
	stree = [0]*(4*n)
	buildTree()
	print(querySum(2,4))
