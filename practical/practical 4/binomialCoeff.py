def binomialCoeff(n):
	table = [[0]*(n+1) for i in range(n+1)]
	for i in range(n+1):
		for r in range(i+1):
			if(i==r|r==0):
				table[i][r] = 1
			else:
				table[i][r] = table[i-1][r-1]+table[i-1][r]
	return table

if __name__ == '__main__':
	n = 6
	table = binomialCoeff(n)
	k = 4
	print(table[n][k])

	