# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

def searchBST(self, root: Optional[TreeNode], val: int) -> Optional[TreeNode]:
    curr_node = root
    while curr_node:
        if curr_node.val == val:
            return curr_node
        elif val>curr_node.val:
            curr_node = curr_node.right
        else:
            curr_node = curr_node.left
    