 def reverse(num):
    rev_num = 0
 
    while (num > 0):

        rev_num = rev_num * 10 + num % 10
        num = num // 10
 
    return rev_num

def countReverse(arr,n):
    res = 0
 
    # Iterate through all pairs
    for i in range(n):
        for j in range(i + 1, n):

            if (reverse(arr[i]) == arr[j]):
                res += 1
 
    return res
 
# Driver code
if __name__ == '__main__':
    a =  [1,3,2,3,1]

    n =  len(a)
    print(countReverse(a, n))