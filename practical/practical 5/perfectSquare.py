def isPerfectSquare(num):
    l,r = 0, num
    while(l<=r):
        mid = (l+r)//2
        squared = mid**2
        if num == squared:
            return True
        elif squared<num:
            l = mid+1
        else:
            r = mid-1
    return False

    