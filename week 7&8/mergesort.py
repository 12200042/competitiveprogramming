def merge(arr, L, R):
	i = j = k = 0

	while(i<len(L) and j<len(R)):
		if L[i] <= R[j]:
			arr[k] = L[i]
			i += 1
		
		else:
			arr[k] = R[j]
			j += 1
		k+=1

	# copy rem elements from left array
	while(i < len(L)):
		arr[k] = L[i]
		i += 1
		k += 1

	# copy rem elements from right array
	while(j < len(R)):
		arr[k] = R[j]
		j += 1
		k += 1

	return arr

def mergeSort(arr):
	size = len(arr)
	# base case
	if size <= 1:
		return
	# rec case
	m = size//2

	L = arr[:m]
	R = arr[m:]

	mergeSort(L)
	mergeSort(R)

	return merge(arr, L, R)


if __name__ == '__main__':
	ar = [2,5,3,6,7,4]

	sorted = mergeSort(ar)
	print(sorted)