# helper function to check if it is possible to divide among k friends
# having atleast min_coins

def kDivide(arr, n, k, min_coins):
	partitions = 0
	coins = 0 

	for i in range(n):
		if coins + arr[i] >= min_coins:
			partitions += 1
			coins = 0 # new partition, coins value starts at zero
		else:
			coins += arr[i]

	return partitions >= k

def BST(arr, n, k):
	s = min(arr) 
	e = sum(arr) # search space from min coin val to sum of all coins

	ans = 0
	while s <= e:
		m = (s+e)//2
		isPossible = kDivide(arr, n, k, m)
		
		if isPossible:
			s = m+1 # increase to coin value, right
			ans = m
		else:
			e = m - 1 # decrease coin value, left

	return ans

if __name__ == '__main__':
	arr = [1, 2, 3, 4]
	n = len(arr)
	k = 3
	print(BST(arr, n, k))
