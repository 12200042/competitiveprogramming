import collections 

class Graph:
    # constructor
    def __init__(self):
        self.graph = collections.defaultdict(list)
        
    def add_edge(self, src, dest):
        self.graph[src].append(dest)
        self.graph[dest].append(src)

    def dfsHelper(self, node, visited):
        visited.add(node) # mark v as visited
        print(node, end = " ")
        for nbr in self.graph[node]:
            if nbr not in visited: # if nbr is not visited
                self.dfsHelper(nbr, visited)
        return

    # wraper function
    def pdfs(self, src):
        visited = set()
        self.dfsHelper(src, visited)

if __name__ == '__main__':
    g = Graph() 
    g.add_edge('A', 'B')
    g.add_edge('A', 'C')
    g.add_edge('A', 'D')
    g.add_edge('B', 'E')
    g.add_edge('E', 'C')
    g.add_edge('C', 'D')
    print(g.graph)
    g.pdfs('A')