from collections import defaultdict, deque

def add_edge(edges):
	adjList = defaultdict(list)
	for src, dest in edges:
		adjList[src].append(dest)
		adjList[dest].append(src)

	return adjList

def bfs(src, graph, dest = -1):
	queue = deque([src])
	visited = set()
	visited.add(src)

	V = len(g)
	dist = [0]*V
	parent = [-1]*V 

	parent[src] = src
	dist[src] = 0

	while queue:
		currentNode = queue.popleft() # remove
		# traverse all adj nodes of currentNode
		for nbr in graph[currentNode]:
			if nbr not in visited:
				queue.append(nbr)
				visited.add(nbr)
				parent[nbr] = currentNode
				dist[nbr] = dist[currentNode]+1

	# print shortest dist from src to node i
	for i in range(0, len(dist)):
		print(str(src)+" to "+str(i)+" is "+ str(dist[i]))

	# find path from src to dest
	path = []
	path.append(dest)

	if dest != -1:
		while dest != src:
			dest = parent[dest]
			path.append(dest)
		
		path.reverse()
		print(path)


if __name__ == '__main__':
	edges = [[0,1], [0,3], [0,5], [1,2], [1,3], [2,3], [3,4], [4,5]]
	g = add_edge(edges)
	bfs(1, g, 5)