def binarySearch(n):
	if n == 1:
		return 1
	l = 1
	r = n

	height = 0
	while l<=r:
		mid = (l+r)//2
		coins_count = (mid*(mid+1))//2
		if coins_count <= n:
			height = mid
			l = mid+1
		else:
			r = mid-1

	return height

if __name__ == '__main__':
	t = int(input())
	while t:
		n = int(input())
		print(binarySearch(n))
		t -= 1