def sortColors( nums):
        l= 0
        i = 0
        r = len(nums)-1
        while i <= r:
            if nums[i] == 0:
                nums[l], nums[i]=nums[i], nums[l]
                l += 1
                i += 1
            elif nums[i] == 2:
                nums[i], nums[r]=nums[r], nums[i]
                r -= 1
            else:
                i += 1

        return nums

if __name__ == '__main__':
    nums = [2,0,2,1,1,0]
    print(sortColors(nums))
    
