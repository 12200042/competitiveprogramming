# construct binary tree
def buildTree(s, e, node, arr):
	# base case
	if (s == e):
		stree[node] = arr[s]
		return 

	m = (s+e)//2

	# build left (s, m)
	buildTree(s, m, 2*node+1, arr)

	# build right (m+1, e)
	buildTree(m+1, e, 2*node + 2, arr)

	# move up from leaf and fill parent nodes/current node
	stree[node] = stree[node*2 + 1] + stree[node*2 + 2]


# find range sum
def query(s, e, l, r, node):
	# no overlapp
	if s > r or e < l:
		return 0

	# complete overlap
	if s >= l and e <= r:
		return stree[node] 

	# partial overlap
	mid = (s+e)//2
	q1 = query(s, mid, l, r, 2*node+1)
	q2 = query(mid+1, e, l, r, 2*node+2)

	return q1+q2	

if __name__ == '__main__':
	arr = [1, 3, 4, 2, 8, 7, 5, 6]
	n = len(arr)
	stree = [0]*(2*n)
	buildTree(0, n-1, 0, arr)
	# print(stree)
	print(query(0, n-1, 3, 5, 0)) # 17
