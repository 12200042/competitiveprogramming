N = 100

table = [[0]*(N+1) for i in range(N+1)]

def binomialCoeff():
	# for row n
	for i in range(N+1):
		for r in range(i+1): # for column r
			if i == r or r == 0:
				# nCn or nC0 = 1
				table[i][r] = 1
			else:
				table[i][r] = table[i-1][r-1] + table[i-1][r]

def catalan(n):
    binomialCoeff() 
    # 2nCn
    c = table[2*n][n]
 
    # 2nCn/(n+1)
    return c//(n + 1)

if __name__ == '__main__':
	n = int(input())
	print(catalan(n))