def binomialCoeff(n):
	table = [[0]*(n+1) for i in range(n+1)]
	# for row n
	for i in range(n+1):
		for r in range(i+1): # for column r
			if i == r or r == 0:
				# nCn or nC0 = 1
				table[i][r] = 1
			else:
				table[i][r] = table[i-1][r-1] + table[i-1][r]

	return table

if __name__ == '__main__':
	N = 777
	table = binomialCoeff(N)
	n = 7
	print(table[n][5]+table[n][6]+table[n][7])