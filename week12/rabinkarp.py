p = 31
mod= 10**9+7
# returns a^n value
def power(a, n):
	result = 1
	while n:
		if n&1:
			result = (result * a)%mod
		a = (a*a)%mod
		n >>=1

	return result

# returns hash value of string s	
def polyHash(s):
	hashValue = 0
	pPow = 1
	for ch in s:
		hashValue= (hashValue+(ord(ch)-ord('a')+1)*pPow)%mod
		pPow = pPow*p
	return hashValue

# prints all the occurrences of patr in s
def rabinKarp(s, patr):
	n = len(s)
	m = len(patr)
	pHash = polyHash(patr) # pattern hash value
	subHash = polyHash(s[:m]) # substring acd
	
	if pHash == subHash:
		print(0)
	
	pModInv = power(p, mod-2)
	pPow = power(p, m-1)
	
	# find substring hash value from index 1
	for i in range(1, n-m+1):
		# remove first character
		subHash = (subHash - (ord(s[i-1])-ord('a')+1)+mod)%mod
		# reduce the power
		subHash = (subHash*pModInv)%mod
		# add new char
		subHash = (subHash + (ord(s[i+m-1])-ord('a')+1)*pPow)%mod
		if subHash == pHash:
			print(i)

if __name__ == '__main__':
	s = "acdecdefcde"
	patr = "cde"
	rabinKarp(s, patr)