import sys
sys.stdin = open("input.txt", "r")
sys.stdout = open("output.txt", "w")
sys.stderr = open("error.txt", "w")
# your remaining code

p = 31
mod = 10**9+7

def power(a, n):
	result = 1
	while n:
		if n & 1: 
			result = (result *a) %mod
		a = (a*a )% mod
		n>>=1
	return result 

def polyHash(s):
	hashValue = 0
	pPow = 1
	for ch in s:
		hashValue = (hashValue + (ord(ch)-ord('a')+1)*pPow)%mod
		pPow = pPow * p

	return hashValue

def rabinKarb(s, patr):
	n = len(s)
	m = len(patr)
	pHash = polyHash(patr)#pattern hash value
	subhash = polyHash(s[:m])
	if pHash == subhash:
		print(0)
	pModIn = power(p, mod-2)

	for i in range(1, n-m+1):
		#remaove the first character
		subhash = (subhash - (ord(s[i-1])-ord('a')+1)+mod)%mod #add mod again to not get a negative number
		
		#reduce the power
		subhash =(subhash * pModIn)%mod 

		#add new character
		#power of last character will one less than the m-1
		subhash = (subhash + (ord(s[i+m-1])-ord('a')+1)*power(p, m-1))%mod

		if subhash == pHash:
			print(i)

if __name__ == '__main__':
	s = 'acdefcde'
	patr = 'cde'
	rabinKarb(s, patr)
